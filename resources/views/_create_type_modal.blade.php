<div class="modal fade text-left" id="createType" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="CategoryLabel">Add New Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="create-category-form" method="POST" class="form form-horizontal" action="{{route('typeStore')}}">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col">
                                <label class="label-control" for="typeName">Name</label>
                                <input type="text" id="typeName" class="form-control @error('name') is-invalid @enderror" name="typeName">
                                <span class="invalid-feedback"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
                        Close
                    </button>

                    <button type="button" id="save_type" class="btn btn-outline-primary">
                        Save Type
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#save_type").on('click', function () {
            var type_name = $("input[name=typeName]").val();
            $.ajax({
                type: 'post',
                url: '/type/store',
                data: {
                    name: type_name
                },
                success: function (response) {
                    $("#createType").modal('hide');
                    $('.modal-backdrop').remove();

                    var option = "<option value=" + response['id'] + ">" + response['name'] + "</option>";
                    $("#type").append(option);

                }
            });

        });
    });

</script>