<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Type;
class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();
        DB::statement('ALTER TABLE types AUTO_INCREMENT = 1');

        $data = [
            ['name' => 'phone'],
            ['name' => 'pad'],
            ['name' => 'laptop']
        ];

        Type::insert($data);
    }
}
