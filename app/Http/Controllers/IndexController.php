<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\Type;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    public function index()
    {
        $typeList = Type::all();
        $products = Cache::remember('products', 60, function () {
            return Product::paginate(10);
        });

        return view('welcome', compact(['typeList', 'products']));
    }
}
