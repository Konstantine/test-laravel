<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductFormRequest;
use App\Models\Product;
use App\Models\ProductAttribute;

class ProductController extends Controller
{
    public function store(CreateProductFormRequest $request)
    {
        $product = new Product();
        $product->sku = $request->sku;
        $product->title = $request->title;
        $product->type_id = $request->type;

        if ($product->save()) {
            $attrs = data_get($request->attr, 'attr');

            foreach ($attrs as $value) {
                $product->attributes()->save(new ProductAttribute(
                    ["attribute" => $value['attribute'], 'value' => $value['value']]
                ));
            }
        }

        return redirect()->back();
    }

}
