<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeRequestForm;
use App\Models\Type;

class TypeController extends Controller
{
   public function store(CreateTypeRequestForm $request)
   {
       return Type::create($request->all());
   }
}
