<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'products_attributes';

    protected $fillable = ['attribute', 'value'];

    public function product()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
