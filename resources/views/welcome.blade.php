<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="{{asset('js/scripts/dynamic-form.js')}}"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}" />



    </head>
    <body>
        <div class="container mt-5">
            <h2>Products</h2>
            <table class="table table-striped mb-5">
                <thead>
                <tr>
                    <th>Product Id</th>
                    <th>Sku</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Attributes</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($products as $key => $product)
                  <tr>
                      <td>{{$product->id}}</td>
                      <td>{{$product->sku}}</td>
                      <td>{{$product->title}}</td>
                      <td>{{$product->type->name}}</td>
                      <td>
                          @foreach($product->attributes as $value)
                              <span class="badge badge-info">{{$value['attribute']}} @if ($value) : @endif {{$value['value']}}</span>
                          @endforeach
                      </td>
                  </tr>
                @empty
                    <p>No products</p>
                @endforelse
                </tbody>
            </table>
            <div class="row">
                {{ $products->links() }}
            </div>

            <h2>Create new Product</h2>
            <div class="card">
                <div class="card-body">
                    <form role="form" method="POST" action="{{route('productStore')}}">
                        @csrf
                        <div class="form-group">
                            <label for="sku">SKU:</label>
                            <input name="sku" id="sku" type="text" class="form-control @error('sku') is-invalid @enderror" value="{{ old('sku') }}" >

                            @error('sku')
                            <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input id="title" name="title" type="text" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}">

                            @error('title')
                            <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="row align-items-end">
                                <div class="col-8">
                                    <label for="type">Select Type:</label>
                                    <select id="type" name="type" class="form-control @error('type') is-invalid @enderror">
                                        <option value="" selected disabled>Select type</option>
                                        @foreach($typeList as $type)
                                            <option @if(old('type') == $type->id) selected @endif value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>

                                    @error('type')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>

                                <div class="col-4">
                                    <button class="btn btn-success" data-toggle="modal"
                                            data-target="#createType" type="button" href="">Add Type</button>
                                </div>
                            </div>
                        </div>
                        <div id="createProductForm" class="form-group">
                            <label class="text-center d-block" for="type">Attribute:</label>
                            <div class="row">
                                <div class="col-4">
                                    <label class="small" for="type">Name:</label>
                                    <input id="attr" name="attribute" type="text" class="form-control @error('attribute') is-invalid @enderror"  value="{{old('attribute')}}">

                                    @error('attribute')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>

                                <div class="col-4">
                                    <label class="small" for="type">Value:</label>
                                    <input id="value" name="value" type="text" class="form-control @error('value') is-invalid @enderror" value="{{old('value')}}">

                                    @error('value')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>

                                <div class="col-4 align-self-end">
                                    <button class="btn btn-sm btn-secondary" id="add">
                                        ADD
                                    </button>

                                    <button class="btn btn-sm btn-danger" id="remove">
                                        REMOVE
                                    </button>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <!-- Create Type Modal -->
                    @include('_create_type_modal')
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    $(document).ready(function () {
        $("#createProductForm").dynamicForm("#createProductForm","#add", "#remove", {
            limit:10,
            formPrefix : "attr",
            normalizeFullForm : false
        });
    });
</script>