<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function type()
    {
        return $this->hasOne('App\Models\Type', 'id', 'type_id');
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\ProductAttribute');
    }


}
